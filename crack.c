#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
//char *md5(const char *str, int length)
int qcompare(const void *a, const void *b)
{
    return strcmp(*((char **)a), *((char **)b));
}

int bcompare(const void *key, const void *elem)
{
    
    //printf("KEY: %s\n", key);
    //printf("Dictonary: %s\n", *((char **)elem));
    return strncmp((char *)key, *((char **)elem), HASH_LEN-1);
}

int NumofStrs(char* filename)
{
    FILE *NumLines;
    NumLines = fopen(filename, "r");
    if(!filename)
    {
        printf("Couldn't open %s\n", filename);
        exit(1);
    }
    int c, lines =0;
    while((c = fgetc(NumLines))!= EOF)
    {
        if(c =='\n')
        {
            lines++;
        }
    }
    return lines;
}

int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    //printf("guess: %s\n",guess);
    char *hashG = md5(guess, strlen(guess));
    // Compare the two hashes
    if(strcmp(hashG,hash)==0)
    {
        return 1;
    }
    else
    {
        //printf("TryGuessHashG: %s\n",hashG);
       // printf("TryGuessHash: %s\n",hash);
        return 0;
    }

    // Free any malloc'd memory
}

// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename, int numstrs)
{
    FILE *hasInp;
    hasInp = fopen(filename, "r");
    if( !filename )
    {
        printf("Couldn't open %s\n", filename);
        exit(1);
    }
    //SPC for Array of char pointers
    char** hashSpace = malloc(numstrs*sizeof(char*));
    //create holder string & scan dict file
    int i = 0;
    char lineBuffer[HASH_LEN+1];
    while (fgets(lineBuffer,sizeof(lineBuffer), hasInp) != NULL)
    {
        //gets rid of NewLine Char
        char *ptr = strtok(lineBuffer,"\n");
        //alloc spc for str
        hashSpace[i] = malloc((strlen(lineBuffer)+1) * sizeof(char));
        //copy str into array
        strcpy(hashSpace[i],ptr);
        i++;
       
        
    }
    return hashSpace;
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.

char **read_dict(char *filename, int numstrs)
{
    FILE *dictInp;
    //SPC for Array of char pointers
    char** dicSpace = malloc(numstrs*sizeof(char*));
    
    //create buffer string & scan dict file
    char lineBuffer[PASS_LEN];
    
    //Open Dictonary
    dictInp = fopen(filename, "r");
    //Cant open file err out
    if( !dictInp )
    {
        printf("Couldn't open %s\n", filename);
        exit(1);
    }
    //Counter for while loop
    int i = 0;
    int lines =0;
    char buffer[PASS_LEN+HASH_LEN+1];
    while (fgets(lineBuffer,sizeof(lineBuffer), dictInp) != NULL)
    {
        //dumps string to pointer in char ** and cats buffer w/dictionary to hashes
        char * hashes = md5(lineBuffer, strlen(lineBuffer)-1);
        //Copies hashes to buffer
        for( int z = 0; z < strlen(hashes)+1; z++)
        {
            if(hashes[z]!='\n')
            {
                buffer[z] = hashes[z];
            }
            
        }
        //Cat divider onto buffer
        strcat(buffer,"/"); 
        //Cat pass to hash
        strcat(buffer, lineBuffer);
        //Set space for entire string +1 for NULL
        dicSpace[i] = malloc((strlen(buffer)+1) * sizeof(char));
        //Gets rid of NewLine Char
        char *ptr = strtok(buffer,"\n");
        strcpy(dicSpace[i],ptr);
        i++;
        lines++;
    }
    printf("Amount of Lines%d\n",lines);
    fclose(dictInp);
    return dicSpace;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    int hlen = NumofStrs(argv[1]);
    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1], hlen);
   
   
    // TODO: Read the dictionary file into an array of strings
    int dlen = NumofStrs(argv[2]);
    char **dict = read_dict(argv[2],dlen);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dlen, sizeof(char *), qcompare);
    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    char buffer[35];
    int i =0;
    int win =0, los =0;
    while(hashes[i] != NULL)
    {
     //Holds current hash for Binary Search   
     char *key = strcpy(buffer, hashes[i]);
     //Buffer to hold match of found and store plain text passwd
     char fbuffer[100];
     //Searches for hash in dictonary
     char **found = bsearch(key, dict, dlen, sizeof(char *), bcompare);
     if (found != NULL)
     {
         //transfer found password in hash to verify output in tryguess fxn
         //printf("Found %s\n", *found);
         strncpy(fbuffer,*found+33, strlen(*found));
         //printf("fbuffer : %s\n",fbuffer);
         //printf("current hash index: %s\n",hashes[i]);
         if(tryguess(hashes[i],fbuffer) == 1)
         {
            printf("Hash: %s Equals Password: %s\n", hashes[i], fbuffer );
            win++;
         }
         else printf("does not equal\n");

         i++;
     }
     else
     {
         //printf("Sorry\n");
         los++;
         i++;
     }
    }
    printf("WINS: %d & LOSSES: %d\n", win, los);
    printf("Num of Lines of Dictonary is: %d & Hashes: %d \n", dlen,hlen);

   
    
}
